import React, {Component} from 'react';

export default class Employee extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employeeList: [],
            showModel: false,
            tempEmployee: {}
        }
        this.toggleModule = this.toggleModule.bind(this);
        this.updateTmpEmployee = this.updateTmpEmployee.bind(this);
    }

    componentDidMount() {
        window.fetch('http://localhost:3200/employee-list', {method: 'GET'}).then(res => res.json()).then(res => {
            this.setState({
                employeeList: res
            })
        })
    }

    toggleModule(p) {
        this.setState({showModel: p});
    }

    updateTmpEmployee(key, data) {
        let obj = this.state.tempEmployee;
        obj[key] = data;
        this.setState({tempEmployee: obj});
    }
    addEmployee(){
        window.fetch('http://localhost:3200/add-employee', {method: 'POST',body:JSON.stringify({employee:this.state.tempEmployee})}).then(res => res.json()).then(res => {
            this.setState({
                employeeList: res,
                tempEmployee:{},
                showModel:false
            })
        })
    }


    render() {
        return (
            <div>
                <h1>
                    List of employee <div className="btn btn-primary float-right"
                                          onClick={() => this.toggleModule(true)}> Add Employee </div>
                </h1>

                <div className="columns">
                    {this.state.employeeList.map((emp, ind) => {
                        return (<div key={ind + "mykey"} className=" column col-6 col-xs-12">
                            <div className=" panel">
                                <div className=" panel-body">
                                    <div className=" tile tile-centered">
                                        <div className=" tile-content">
                                            <div className=" tile-title text-bold">Name</div>
                                            <div className=" tile-subtitle">{emp.name}</div>
                                        </div>
                                    </div>
                                    <div className=" tile tile-centered">
                                        <div className=" tile-content">
                                            <div className=" tile-title text-bold">Employee ID</div>
                                            <div className=" tile-subtitle">{emp.empId}</div>
                                        </div>
                                    </div>
                                    <div className=" tile tile-centered">
                                        <div className=" tile-content">
                                            <div className=" tile-title text-bold">Salary</div>
                                            <div className=" tile-subtitle">{emp.salary}</div>
                                        </div>
                                    </div>
                                    <div className=" tile tile-centered">
                                        <div className=" tile-content">
                                            <div className=" tile-title text-bold">Business Unit</div>
                                            <div className=" tile-subtitle">{emp.bu}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>)
                    })
                    }
                </div>

                {
                    this.state.showModel && (<div className="modal active" id="modal-id">
                        <a href="javascript:void(0)" className="modal-overlay" aria-label="Close"
                           onClick={() => this.toggleModule(false)} />
                        <div className="modal-container">
                            <div className="modal-header">
                                <div className="btn btn-clear float-right" aria-label="Close" />
                                <div className="modal-title h5">Add Employee</div>
                            </div>
                            <div className="modal-body">
                                <div className="content">
                                    <div className="docs-demo columns">
                                        <div className="column col-6 col-xs-12">
                                            <div className="form-group">
                                                <label className="form-label" htmlFor="input-example-1">Name</label>
                                                <input className="form-input" id="input-example-1" type="text"
                                                       placeholder="Name"
                                                       onChange={(e) => this.updateTmpEmployee('name', e.target.value)}/>
                                            </div>
                                            <div className="form-group">
                                                <label className="form-label" htmlFor="input-example-2">Employee ID</label>
                                                <input className="form-input" id="input-example-2" type="text"
                                                       placeholder="Employee ID"
                                                       onChange={(e) => this.updateTmpEmployee('empID', e.target.value)}/>
                                            </div>
                                            <div className="form-group">
                                                <label className="form-label" htmlFor="input-example-3">Salary</label>
                                                <input className="form-input" id="input-example-3" type="text"
                                                       placeholder="Salary"
                                                       onChange={(e) => this.updateTmpEmployee('salary', e.target.value)}/>
                                            </div>
                                            <div className="form-group">
                                                <label className="form-label" htmlFor="input-example-4">Business
                                                    Unit</label>
                                                <input className="form-input" id="input-example-4" type="text"
                                                       placeholder="Business Unit"
                                                       onChange={(e) => this.updateTmpEmployee('bu', e.target.value)}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <div className="btn btn-primary" onClick={() => this.addEmployee()}>Add</div>
                            </div>
                        </div>
                    </div>)
                }
            </div>
        )
    }
}
